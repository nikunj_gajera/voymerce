package com.cs.SpeechAnalyzer.utils;

import java.io.Serializable;
import java.util.Date;

public class ResponseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Object data;
	
	private String message;
	
	private Boolean status;
	
	private Date timestamp;
	
	public ResponseBean() {}
	
	public ResponseBean(Boolean status, String message, Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
		this.timestamp = new Date();
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getTimestamp() {
		return new Date();
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
