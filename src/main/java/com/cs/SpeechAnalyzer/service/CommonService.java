package com.cs.SpeechAnalyzer.service;

import com.cs.SpeechAnalyzer.beans.SpeechBean;
import com.cs.SpeechAnalyzer.utils.ResponseBean;

public interface CommonService {

	public ResponseBean handleSpeechRecognize(SpeechBean bean);

}
