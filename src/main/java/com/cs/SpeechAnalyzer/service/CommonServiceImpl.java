package com.cs.SpeechAnalyzer.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cs.SpeechAnalyzer.beans.SpeechBean;
import com.cs.SpeechAnalyzer.utils.ResponseBean;
import com.google.cloud.speech.v1p1beta1.RecognitionAudio;
import com.google.cloud.speech.v1p1beta1.RecognitionConfig;
import com.google.cloud.speech.v1p1beta1.RecognitionConfig.AudioEncoding;
import com.google.cloud.speech.v1p1beta1.RecognizeResponse;
import com.google.cloud.speech.v1p1beta1.SpeechClient;
import com.google.cloud.speech.v1p1beta1.SpeechRecognitionAlternative;
import com.google.cloud.speech.v1p1beta1.SpeechRecognitionResult;
import com.google.protobuf.ByteString;

@Service
public class CommonServiceImpl implements CommonService {

	@Override
	public ResponseBean handleSpeechRecognize(SpeechBean bean) {

		try {
			SpeechClient speech = SpeechClient.create();
			File file = new File(bean.getFiles().getOriginalFilename());
			//File file = convert(bean.getFiles());
			if(!file.exists())
				file.createNewFile();
			/*File cfile = new File("c"+bean.getFiles().getOriginalFilename());
			if(!cfile.exists())
				cfile.createNewFile();*/
			OutputStream outputStream = new FileOutputStream(file);
			IOUtils.copy(bean.getFiles().getInputStream(), outputStream);
			outputStream.close();
			//mp3ToWav(file);
			Path path = Paths.get(file.getAbsolutePath());
			byte[] data = Files.readAllBytes(path);
			ByteString audioBytes = ByteString.copyFrom(data);
			
			RecognitionConfig config = null;
			//Handle flac format audio file
			if(file.getAbsolutePath().endsWith(".flac")) {
				
				config = RecognitionConfig.newBuilder()
						.setEncoding(AudioEncoding.FLAC)
						.setLanguageCode("en-US").setSampleRateHertz(44100)
						.setEnableAutomaticPunctuation(true)
						.setModel("default")
						.build();
				
			//Handle wav format audio file
			} else if(file.getAbsolutePath().endsWith(".wav")) {
				config = RecognitionConfig.newBuilder()
						.setEncoding(AudioEncoding.LINEAR16)
						.setLanguageCode("en-US").setSampleRateHertz(16000)
						.setEnableAutomaticPunctuation(true)
						.setModel("default")
						.build();
			} else if(file.getAbsolutePath().endsWith(".ogg")) { 
				config = RecognitionConfig.newBuilder()
						.setEncoding(AudioEncoding.OGG_OPUS)
						.setLanguageCode("en-US").setSampleRateHertz(48000)
						.setEnableAutomaticPunctuation(true)
						.setModel("default")
						.build();
			}else {
				config = RecognitionConfig.newBuilder()
						.setEncoding(AudioEncoding.LINEAR16)
						.setLanguageCode("en-US").setSampleRateHertz(16000)
						.setModel("default")
						.setEnableAutomaticPunctuation(true)
						.build();
			}
			
			RecognitionAudio audio = RecognitionAudio.newBuilder()
					.setContent(audioBytes).build();
			// Use blocking call to get audio transcript
			RecognizeResponse response = speech.recognize(config, audio);
			List<SpeechRecognitionResult> results = response
					.getResultsList();
			String speechText = "";
			for (SpeechRecognitionResult result : results) {
				// There can be several alternative transcripts for a given
				// chunk of speech. Just use the
				// first (most likely) one here.
				SpeechRecognitionAlternative alternative = result
						.getAlternativesList().get(0);
				speechText +=	alternative.getTranscript()+" ";
			}
			file.delete();
			
			Map<String, Object> resMap = new HashMap<String, Object>();
			List<String> pids = new ArrayList<String>();
			pids.add("SKU000001");
			pids.add("SKU000005");
			pids.add("SKU000009");
			
			resMap.put("displayResponseText", speechText);
			resMap.put("displayProductIDs", pids);
			return new ResponseBean(true, "", resMap);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBean(false, "", e.getMessage());
		}
	}

	public static File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
	
	public static void mp3ToWav(File oggData) throws UnsupportedAudioFileException, IOException {
	    // open stream
	    AudioInputStream mp3Stream = AudioSystem.getAudioInputStream(oggData);
	    // create audio format object for the desired stream/audio format
	    // this is *not* the same as the file format (wav)
	    AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 
	        16000, 16, 
	        1, 
	        1,
	        16000,
	        false);
	    // create stream that delivers the desired format
	    AudioInputStream converted = AudioSystem.getAudioInputStream(convertFormat, mp3Stream);
	    // write stream into a file with file format wav
	    AudioSystem.write(converted, Type.WAVE, new File("cblob.wav"));//(converted, Type.WAVE, new File("C:\\temp\\out.wav"));
	}	
}
