package com.cs.SpeechAnalyzer.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cs.SpeechAnalyzer.beans.SpeechBean;
import com.cs.SpeechAnalyzer.service.CommonService;
import com.cs.SpeechAnalyzer.utils.ResponseBean;

@RestController
@CrossOrigin(origins = "*")
public class CommonController {

	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ResponseBean handleSpeechRecognize(HttpServletRequest req) {
		return new ResponseBean(true, "Welcome visitors to Voymerce Webservices", "");
	}

	@RequestMapping(value="/speech/recognize", method=RequestMethod.POST)
	public ResponseBean handleSpeechRecognize(SpeechBean bean, HttpServletRequest req) {
		
		return commonService.handleSpeechRecognize(bean);
	}
}
	
