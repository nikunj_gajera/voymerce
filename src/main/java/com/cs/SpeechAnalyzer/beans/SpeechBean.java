package com.cs.SpeechAnalyzer.beans;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class SpeechBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private MultipartFile files;

	public MultipartFile getFiles() {
		return files;
	}

	public void setFiles(MultipartFile files) {
		this.files = files;
	}
}
